Objective: 
Today we reviewed the codes written last night. During this process, I am clearer of the concept of side effect and React.
In the afternoon, we did a presentation, during which I learned from the other two groups and found my own deficiencies.

Reflective:
I felt benefit greatly from the presentations that were shown by teammates from the other two groups. When listening to others’ explanations, I learned a lot.

Interpretive:
A good presentation is generated from the wisdom of one group of people, from which I can gain knowledge effectively.

Decisional:
I decided to learn more seriously and try to perform more better in the future presentation.
