import { configureStore } from '@reduxjs/toolkit'
import todoListSlice from '../components/todoListSlice';

export default configureStore({
  reducer: {
    todoList: todoListSlice,
  },
});