import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";

const TodoDetail = () => {
    const params = useParams();
    const navigate = useNavigate();
    const { id } = params;
    const todo = useSelector((state) => {
        return state.todoList.todoList.find((todo) => parseInt(todo.id) === parseInt(id))
    })

    useEffect(() => {
        if (!todo) {
            navigate("/404")
        }
    }, [todo, navigate])

    return ( 
        <>
            <div>Detail</div>
            <p>{todo?.text}</p>
        </>
    );
}
 
export default TodoDetail;