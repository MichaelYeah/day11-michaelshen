import { useSelector } from "react-redux";
import TodoGroup from "../../components/TodoGroup";

const DoneList = () => {
    const todos = useSelector((state) => state.todoList.todoList)
    const doneList = todos.filter(todo => {
        return todo.done;
    })
    return ( 
        <div>
            <TodoGroup todoList={doneList} onlyDone={true} />
        </div>
    );
}
 
export default DoneList;