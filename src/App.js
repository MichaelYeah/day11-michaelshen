import * as React from "react";
import { RouterProvider } from 'react-router-dom';
import './App.css';
// import TodoList from './components/TodoList';
import { router } from './router/router'

function App() {
  return (
    <div className="App">
      <RouterProvider router={router} />
      {/* <TodoList /> */}
    </div>
  );
}

export default App;
