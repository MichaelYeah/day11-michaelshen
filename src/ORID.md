Objective:
In the morning, we learned about Cloud Native, which is upgraded based on Cloud Computing. Then, we did a summary of Spring Boot by drawing concept map together with our teammates.
In the afternoon, we learned about HTML, CSS, JS and React. When learning CSS, we played a frog game. When learning React, we did some little projects.

Reflective:
I benefited greatly from the learning of Cloud Native.
I felt that React makes me confused, especially the props and methods transformed among components.

Interpretive:
Last week, we just finished the representation of Cloud Native. But actually I am not that clear about every points because they are so hard to understand.
For I am not familiar with front-end development for long time, I am puzzled by React, especially the props transforming.

Decisional:
I decided to learn more about React and I will never give up no matter how tough the tasks will be.
