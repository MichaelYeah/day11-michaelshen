import { useDispatch } from 'react-redux';
import { loadTodos } from '../apis/todo1';
import { init } from '../components/todoListSlice';
import { useRef } from 'react';

export const useTodo = () => {
    const dispatch = useDispatch();

    const reloadTodos = () => {
        loadTodos().then((response) => {        
            console.log(response);    
            dispatch(init(response.data))
        });
    }

    return useRef({reloadTodos}).current
}