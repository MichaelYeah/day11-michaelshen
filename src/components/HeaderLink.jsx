import './HeaderLink.css'
import { Link } from "react-router-dom";

const HeaderLink = () => {
    return ( 
        <>
            <Link to="/" >HOME</Link>
            <Link to="/about" >ABOUT</Link>
            <Link to="/done" >DONE</Link>
        </>
     );
}
 
export default HeaderLink;