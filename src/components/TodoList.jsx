import TodoGroup from "./TodoGroup";
import TodoGenerator from "./TodoGenerator";
import './TodoGenerator.css';
import { useSelector } from "react-redux";
import { useEffect } from "react";
import { useTodo } from "../hooks/useTodo";

const TodoList = () => {
    const todoList = useSelector(state => state.todoList.todoList)
    const { reloadTodos } = useTodo()

    useEffect(() => {
        reloadTodos();
    }, [])

    return (
        <div className="todoList">
            <div className="title">Todo List</div>
            <TodoGroup todoList={todoList} />
            <TodoGenerator />
        </div>
    )
}

export default TodoList;