import { useState } from "react";
import './TodoGenerator.css';
import { addTodo } from "../apis/todo";
import { Button, Input } from 'antd';
import { useTodo } from '../hooks/useTodo';

const TodoGenerator = () => {
    const [content, setContent] = useState('');
    const { reloadTodos } = useTodo();

    const ENTER = 13;

    function onInputChange(e) {
        setContent(e.target.value);
    }

    async function handleAddBtnClick() {
        await addTodo({
            text: content,
            done: false
        })
        reloadTodos();
        setContent('');
    }

    function handleKeyUp(event) {
        if (event.keyCode === ENTER) {
            handleAddBtnClick();
        }
    }

    return (
        <div className="todoGenerator">
            <Input className="addInput" value={content} placeholder="input something..." onChange={onInputChange} onKeyUp={handleKeyUp} />
            <Button className="addBtn" type="primary" onClick={handleAddBtnClick} >Add</Button>
        </div>
    )
}

export default TodoGenerator