import './TodoItem.css';
import { useNavigate } from 'react-router-dom';
import { deleteTodo, toggleTodo, updateTodoText } from '../apis/todo';
import { useState } from 'react';
import { Modal, Input, Button } from 'antd';
import { useTodo } from '../hooks/useTodo';

const TodoItem = ({value, onlyDone}) => {
    const navigate = useNavigate();
    const [isEditModalOpen, setIsEditModalOpen] = useState(false);
    const [loading] = useState(false);
    const [editing, setEditing] = useState(value.text);
    const [tempText, setTempText] = useState(value.text);

    const { TextArea } = Input;

    const { reloadTodos } = useTodo();

    async function handleCloseClick() {
        await deleteTodo(value.id);
        reloadTodos();
    }

    async function handleContentClick() {
        if (!onlyDone) {
            await toggleTodo(value.id, !value.done);
            reloadTodos();
        } else {
            navigate(`/todo/${value.id}`);
        }
    } 
    
    async function handleConfirmEdit() {
        await updateTodoText(value.id, editing)
        reloadTodos();
        setTempText(editing);
        setIsEditModalOpen(false);
    };
    
    const handleCancel = () => {
        setEditing(tempText);
        setIsEditModalOpen(false);
    };

    function handleEditBtnClick() {
        setIsEditModalOpen(true);
    }

    function handleEditInputChange(e) {
        const newText = e.target.value;
        setEditing(newText);
    }

    return (
        <div className='todoItem'>
            <div className={value.done ? "content contentDone" : "content"} >
                <span type='text' className='input' onClick={handleContentClick}>{value.text}</span>
                <div className='buttonBox'>
                    <span className='btn editBtn' onClick={handleEditBtnClick}/>
                    <span className='btn closeBtn' onClick={handleCloseClick}/>
                </div>
            </div>
            <Modal title="Edit Text" open={isEditModalOpen} onOk={handleConfirmEdit} onCancel={handleCancel}
                footer={[
                    <Button key="submit" type="primary" loading={loading} onClick={handleConfirmEdit}>confirm</Button>,
                    <Button key="cancel" onClick={handleCancel}>cancel</Button>,
                ]}>
                <TextArea className='editInput' rows={4} value={editing} onChange={handleEditInputChange} />
            </Modal>
        </div>
    )
}

export default TodoItem;