import TodoItem from "./TodoItem"
import './TodoItem.css';

const TodoGroup = ({todoList, onlyDone}) => {
    const generateTodoItems = todoList.map((item) => {
        return (
            <TodoItem key={item.id} value={item} onlyDone={onlyDone} />
        )
    });
    return (
        <div className="todoGroup">
            {generateTodoItems}
        </div>
    )
}

export default TodoGroup