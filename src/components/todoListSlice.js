import { createSlice } from "@reduxjs/toolkit"

const todoListSlice = createSlice({
    name: "todoList",
    initialState: {
        todoList: []
    },
    reducers: {
        init: (state, action) => {
            state.todoList = action.payload
        },
    }
})

export const { init } = todoListSlice.actions

export default todoListSlice.reducer