import { Outlet } from "react-router-dom";
import HeaderLink from "./HeaderLink";

const Layout = () => {
    return ( 
        <>
            <HeaderLink />
            <Outlet /> 
        </>
    );
}
 
export default Layout;