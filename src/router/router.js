import { createBrowserRouter } from "react-router-dom";
import TodoList from "../components/TodoList";
import AboutPage from "../components/About";
import DoneList from "../pages/todo/DoneList";
import Layout from "../components/Layout";
import TodoDetail from "../pages/todo/TodoDetail";
import NotFoundPage from "../pages/todo/NotFoundPage";

export const router = createBrowserRouter([
    {
        path: "/",
        element: (
            <Layout />
        ),
        children: [
            {
                index: true,
                element: (
                  <TodoList />
                ),
            },
            {
                path: "about",
                element: <AboutPage />,
            },
            {
                path: "done",
                element: <DoneList />,
            },
            {
                path: "todo/:id",
                element: <TodoDetail />,
            },
            {
                path: '*',
                element: <NotFoundPage />
            }
        ]
    }
]);
