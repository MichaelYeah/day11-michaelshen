import request1 from "./request1"
export const loadTodos = () => {
    return request1.get("/todos");
}

export const toggleTodo = (id, done) => {
    return request1.put(`/todos/${id}`, {
        done,
    });
}

export const addTodo = (todo) => {
    return request1.post(`/todos`, todo);
}

export const deleteTodo = (id) => {
    return request1.delete(`/todos/${id}`);
}

export const updateTodoText = (id, text) => {
    return request1.put(`/todos/${id}`, {
        text
    })
}