import axios from "axios";

const request = axios.create({
    baseURL: "https://64c0c54f0d8e251fd1128853.mockapi.io/"
});

export default request;